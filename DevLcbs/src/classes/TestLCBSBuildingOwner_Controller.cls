public class TestLCBSBuildingOwner_Controller{
    public Building_Owner__c BuldENew {get;set;}
    public Building__c build {get;set;}
     public string Address1 {get;set;}
    public string Address2 {get;set;}
    public string Address3 {get;set;}
    public List<Messaging.SingleEmailMessage> mails {get;set;}
    public TestLCBSBuildingOwner_Controller(ApexPages.StandardController controller) {
        String BuildingId = System.currentPageReference().getParameters().get('Id');
        BuldENew = new Building_Owner__c();
        build=[select id,name,Building_Owner__c,Building_Owner__r.Email_Address__c,Building_Owner__r.Phone_Number__c from Building__c where id=:BuildingId];
        system.debug('###building'+build);
    }

    
    public list<Building_Owner__c> lstBuildingOwner {get;set;}
    public string sBuildOwner{get;set;}
    public List<SelectOption> lstExtOwn {get;set;}
    String TabInFocus = System.currentPageReference().getParameters().get('tab');
    String BuildingId = System.currentPageReference().getParameters().get('Id');
    
    public TestLCBSBuildingOwner_Controller(){
        lstBuildingOwner = new list<Building_Owner__c>();
        
    }
    
        
    public List<SelectOption>  getBuildingOwners(){
        lstBuildingOwner = [Select id, Name from Building_Owner__c];
        system.debug('lstBuildingOwner++++'+lstBuildingOwner);
        List<SelectOption> BuildingOwners= new List<SelectOption>();
        for(Building_Owner__c s:lstBuildingOwner){
            
            BuildingOwners.add(new SelectOption(s.Name,s.Name));
        }        
        return BuildingOwners;
    }
    
    public String getTabInFocus() {
         System.debug(' *****Current Selected tab is :' + TabInFocus);
    
    return TabInFocus;
    }
    
    public void setTabInFocus( String s ) {
        this.TabInfocus = s;
    }
    Private String var = 'photos';
    public void setVar(String n) {
        var = n;
    }
    
    public String getVar() {
        return var;
    }
    public String setActiveTab(){
            String para = ApexPages.CurrentPage().getParameters().get('var');        
            System.debug('current tab is ' + para);
            return para;
        }
        
      public PageReference buildingOwnerSave() {
       Boolean isTrue = false;
        try{   
            BuldENew.Address__c = Address1+Address2+Address3;
            if(BuldENew != Null){  
                try{
                insert BuldENew;
                isTrue = true;
                
                }catch(Exception e){} 
            }
           // BuldENew.Address_1__c = 'test';
           // update BuldENew;
            build.Building_Owner__c = BuldENew.Id;
            update build;
            system.debug('Naga Test###New Building owner'+BuldENew);
           if(isTrue == true){
           sendEmail();       
             }
        }
        catch(DmlException e){
            String error = e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
            
        }   
       
        if( BuldENew != null){  
         system.debug('>>>>>>>>>');  
       // update BuldENew;   
      //  sendEmail(); 
            }    
        //sendEmail();  
        pagereference p = new pagereference('/LCBS_MoreInfo_BuildInfo?id='+BuildingId); 
        p.setredirect(true);
        return p;                                               
    }   
    
     public void sendEmail(){
        EmailTemplate myEMailTemplate = [Select id,name from EmailTemplate where Name ='LCBS_BuildingAccessReqTemplate'];
            //Mail Functionality added by Chiranjeevi
            mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
           
            Contact c = [select id, Email from Contact where email <> null limit 1];
            mail.setTemplateId(myEMailTemplate.id);
            List<String> sendTo = new List<String>();
            sendTo.add(BuldENew.Email_Address__c);
            system.debug('Email address@@@@@@@@@:'+BuldENew.Email_Address__c);
            mail.setToAddresses(sendTo);
            mail.setTargetObjectId(c.id);           
            mail.setSenderDisplayName('LCBS Building Approval');
            mail.setReplyTo('approval_for_building@1lpcig5s4p7ux2ltna158pjo0mknx7t77fw3y7pndbub6bpijt.g-3nq6jeac.cs17.apex.sandbox.salesforce.com');
            string emailMessage ='Your heating and cooling contractor is requesting data access to your building'+ ' '+build.Name+'.  To grant them access, please reply "Approved" to this mail. To reject them access, please reply "Reject" to this mail <br/><br/><br/> Thank you.';
            mail.whatid = build.id;
            mails.add(mail);
            system.debug('mails ########:'+mails);
            Savepoint sp = Database.setSavepoint();
            system.debug('mails ^^^^^^^:'+mails);
            if(mails.size()>0){
            Messaging.sendEmail(mails);
            Database.rollback(sp);
            
             List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
             for (Messaging.SingleEmailMessage email : mails) {
             system.debug('inside the for loop');
             Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
             emailToSend.setToAddresses(email.getToAddresses());
             emailToSend.setPlainTextBody(email.getPlainTextBody());
             emailToSend.setHTMLBody(email.getHTMLBody());
             emailToSend.setSubject(email.getSubject());
             lstMsgsToSend.add(emailToSend);
             }
            Messaging.sendEmail(lstMsgsToSend);
            /*
            Task task = new Task();
            task.WhatId = build.id;
           // task.WhoId = BuldENew.Technician__c;
            task.Subject = 'Email Sent to Building Owner';
            task.status = 'Completed';
           // task.Description ='TIME :   '+system.now() + '\r\n'+ '\r\n' + 'APPROVER DETAILS' + '\r\n' + 'NAME :  ' + build.Building_Owner__r.name +'\r\n'+ 'EMAIL :  ' + build.Building_Owner__r.Email_Address__c + '\r\n' + ' PHONE NUMBER :   ' + build.Building_Owner__r.Phone_Number__c;
            task.ActivityDate = Date.today();
            insert task;  
            */
            }
       }     
           
  
           
    public PageReference buildingOwnerCancel() {
        pageReference p = new pageReference('/LCBS_MoreInfo_BuildInformation?id='+BuildingId);
        p.setRedirect(true);
        return p;                                               
    }     
    public pageReference EbuildingOwnerSave()
    {
      try{   
            
            Building_Owner__c building = [select id,name,Email_Address__c from Building_Owner__c where name=:sBuildOwner limit 1];
            build.Building_Owner__c = building.id;
            Update build;
            
            EmailTemplate myEMailTemplate = [Select id,name from EmailTemplate where Name ='LCBS_BuildingAccessReqTemplate'];
            //Mail Functionality added by Chiranjeevi
            mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
           
            Contact c = [select id, Email from Contact where email <> null limit 1];
            mail.setTemplateId(myEMailTemplate.id);
            List<String> sendTo = new List<String>();
            sendTo.add(building.Email_Address__c);
            system.debug('Email address@@@@@@@@@:'+building.Email_Address__c);
            mail.setToAddresses(sendTo);
            mail.setTargetObjectId(c.id);           
            mail.setSenderDisplayName('LCBS Building Approval');
            mail.setReplyTo('approval_for_building@1lpcig5s4p7ux2ltna158pjo0mknx7t77fw3y7pndbub6bpijt.g-3nq6jeac.cs17.apex.sandbox.salesforce.com');
            string emailMessage ='Your heating and cooling contractor is requesting data access to your building'+ ' '+build.Name+'.  To grant them access, please reply "Approved" to this mail. To reject them access, please reply "Reject" to this mail <br/><br/><br/> Thank you.';
            mail.whatid = build.id;
            mails.add(mail);
            system.debug('mails ########:'+mails);
            Savepoint sp = Database.setSavepoint();
            system.debug('mails ^^^^^^^:'+mails);
            if(mails.size()>0){
            Messaging.sendEmail(mails);
            Database.rollback(sp);
            
             List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
             for (Messaging.SingleEmailMessage email : mails) {
             system.debug('inside the for loop');
             Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
             emailToSend.setToAddresses(email.getToAddresses());
             emailToSend.setPlainTextBody(email.getPlainTextBody());
             emailToSend.setHTMLBody(email.getHTMLBody());
             emailToSend.setSubject(email.getSubject());
             lstMsgsToSend.add(emailToSend);
             }
            Messaging.sendEmail(lstMsgsToSend);
        }
        }
        catch(DmlException e){
            String error = e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
            
        }        
        pagereference p = new pagereference('/LCBS_MoreInfo_BuildInfo?id='+BuildingId); 
        p.setredirect(true);
        return p;      
    
}
}