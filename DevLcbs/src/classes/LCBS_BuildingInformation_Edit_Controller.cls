/************************************
Name         : LCBS_BuildingInformation_Edit_Controller
Created By   : Chiranjeevi Gogulakonda
Company Name : NTT Data
Project      : LCBS
Created Date : 
Test Class   : 
Test Class URL: 
Usages       : 
Modified By  :
***********************************/

public class LCBS_BuildingInformation_Edit_Controller
{ 
    


    public Building__c fetchinfo {set;get;}
    Id BuldId;
    public boolean isDistributor {get;set;}
    public String PAGE_CSS { get; set; }
    public String site {get; set;}
    public List<String> addr {get;set;}
    public Contact userId1 {get;set;}
    public Contact userId2 {get;set;}
    public Contact userId3 {get;set;}
    public Contact userId4 {get;set;} 
    public Contact userId5 {get;set;}
    public User u {get;set;}
    public List<string> Address {get;set;}
    public string Address1 {get;set;}
    public string Address2 {get;set;}
    public string Address3 {get;set;}
    public user c {get;set;}
    
     set<id> conIds;
    public LCBS_BuildingInformation_Edit_Controller() 
    {
          
          u = [select id,Contactid,AccountId,Contact.AccountId,Account.EnvironmentalAccountType__c from User where id=:userinfo.getuserId()];
          if(u.Account.EnvironmentalAccountType__c == 'Distributor')
          {
           isDistributor = true;
          }
          else if(u.Account.EnvironmentalAccountType__c == 'Contractor')
          {
           isDistributor = false;
          }
                    PAGE_CSS = LCBSParameters.PAGE_CSS_URL;
        
        BuldId = apexpages.currentpage().getparameters().get('id'); 
        if ( BuldId != null ) {
            fetchinfo  =[Select Id,Name,Address__c,Address_1__c,Address_2__c,Alternate_Dispatcher__c,Alternate_Technician__c,City__c,Contractor_Company_Location__c,Country__c,
                         Dispatcher__c,Building_Owner__r.name,Email_Address__c,First_Name__c,Last_Name__c,Number_of_Devices__c,Phone_Number__c,Postal_Code__c,Site__c,State__c,Technician__c,Time_Zone__c,Title__c,Site__r.name,Dispatcher__r.name,Technician__r.name,Contractor_Company_Location__r.name,Service_Level__c,User_5__c,CreatedBy.id from Building__c where id =: BuldId ];
        }
         c = [select id,Contact.Account.id from user where id=:fetchinfo.Createdby.id];
        conIds = new set<id>();
         Map<Id,User> portalUserMap = new Map<Id,User>([SELECT AccountId,ContactId,Email,FirstName,Id,IsActive,IsPortalEnabled,LastName,ProfileId,UserRoleId,UserType FROM User WHERE IsPortalEnabled = true]);
         for(user ul:portalUserMap.values())
             { 
              conIds.add(ul.ContactId);   
             
             }
        // fetchinfo.Time_Zone__c = '';
       /*  String Addr = fetchinfo.Address__c;
          Address = Addr.split(',');
          //system.debug('+++'+Address[2]);
          string addr1 = Address[0];
          if(addr1 == NULL)
          {
              Address1 = '';
          }else{
              Address1 = addr1;
          }
          
          string addr2 = Address[1];
          if(addr2 == NULL)
          {
              Address2 = '';
          }else{
              Address2 = addr2;
          }
          
          string addr3 = Address[2];
          if(addr3 == NULL)
          {
              Address3 = '';
          }else{
              Address3 = addr3;
          }
          */
          /*if(Address[1]!=NULL)
          {
          Address2 = Address[1];
          }
          if(Address[2]!=NULL)
          {
          Address3 = Address[2];
          for(integer i =3;i<Address.size();i++)
          { 
           Address3 += Address[i];
          }
          
        }*/
       
    }
     
    
      public void resetUser1()
    {
     fetchinfo.Dispatcher__c = NULL;
     userId1.id = NULL;
    }
    public void resetUser2()
    {
     fetchinfo.Alternate_Dispatcher__c= NULL;
     userId2.id = NULL;
    }
    public void resetUser3()
    {
     fetchinfo.Technician__c=NULL;
     userId3.id = NULL;
    }
    public void resetUser4()
    {
     fetchinfo.Alternate_Technician__c = NULL;
     userId4.id = NULL;
    }
    public void resetUser5()
    {
     fetchinfo.User_5__c= NULL;
     userId5.id = NULL;
    }
   
    
    
    public PageReference returnBuilding() {
        pageReference p = new PageReference(LCBSParameters.DISTRIBUTOR_HOME_URL);
        p.setRedirect(true);
        return p;
    }
    
    public SelectOption[] getSiteOptions() 
    { 
      
            SelectOption[] getSiteNames= new SelectOption[]{};  
                getSiteNames.add(new SelectOption('','--None--'));
            for (Site__c s : [select id,Name, Contractor_Company_Location1__c from Site__c order by name]) 
            {  
                getSiteNames.add(new SelectOption(s.id,s.Name));   
            }
            return getSiteNames; 
       
    }
    
    public SelectOption[] getContractorCompanyLocOptions() 
    { 
       
            SelectOption[] getContractorCompanyLocNames= new SelectOption[]{};  
                getContractorCompanyLocNames.add(new SelectOption('','--None--'));
            for (Company_Location__c c : [select id,Name from Company_Location__c order by name]) 
            {  
                getContractorCompanyLocNames.add(new SelectOption(c.id,c.Name));
            }
            return getContractorCompanyLocNames; 
        
    }
    
    public SelectOption[] getPrimaryDispatcherOptions() 
    { 
            SelectOption[] getPrimaryDispatcherNames= new SelectOption[]{};  
            getPrimaryDispatcherNames.add(new SelectOption('','Select'));        
            for (Contact co : [select id,Name,role__c from Contact  where Id in : conIds AND AccountID=:u.Contact.AccountId  order by name]) //role__c = 'Primary Dispatcher' and 
            {  
                if( (userId2!=null && userId2.Id == co.Id) || (userId3!=null && userId3.Id == co.Id) || (userId4!=null && userId4.Id == co.Id) ||(userId5!=null && userId5.Id == co.Id) ) {
                    continue;
                }
                getPrimaryDispatcherNames.add(new SelectOption(co.id,co.Name+' '+'('+co.Role__c+')'));
            }
            return getPrimaryDispatcherNames; 
        
    }
    
    public SelectOption[] getSeconDispatcherOptions() 
    { 
        
            SelectOption[] getSeconDispatcherNames= new SelectOption[]{};  
            getSeconDispatcherNames.add(new SelectOption('','Select'));
            for (Contact co : [select id,Name,role__c from Contact where Id in : conIds AND AccountID=:u.Contact.AccountId  order by name]) //role__c ='Secondary Dispatcher' and
            {  
                if( (userId1!=null && userId1.Id == co.Id) || (userId3!=null && userId3.Id == co.Id) || (userId4!=null && userId4.Id == co.Id) ||(userId5!=null && userId5.Id == co.Id) ) {
                    continue;
                }
                getSeconDispatcherNames.add(new SelectOption(co.id,co.Name+' '+'('+co.Role__c+')'));
                
            }
            return getSeconDispatcherNames; 
       
    }
    
    public SelectOption[] getPrimaryTechOptions() 
    { 
         SelectOption[] getPrimaryTechNames= new SelectOption[]{};  
                getPrimaryTechNames.add(new SelectOption('','Select'));
                
            for (Contact co : [select id,Name,role__c  from Contact where Id in : conIds AND AccountID=:u.Contact.AccountId  order by name]) //role__c = 'Primary Technician' and 
            {  
                if( (userId1!=null && userId1.Id == co.Id) || (userId2!=null && userId2.Id == co.Id) || (userId4!=null && userId4.Id == co.Id) ||(userId5!=null && userId5.Id == co.Id) ) {
                    continue;
                }
                getPrimaryTechNames.add(new SelectOption(co.id,co.Name+' '+'('+co.Role__c+')'));
            }
            return getPrimaryTechNames; 
       
    }
    
    public SelectOption[] getSecondaryTechOptions() 
    { 
        
            SelectOption[] getSecondaryTechNames= new SelectOption[]{};  
                getSecondaryTechNames.add(new SelectOption('','Select'));
               
                
            for (Contact co : [select id,Name,role__c  from Contact where Id in : conIds AND AccountID=:u.Contact.AccountId order by name]) //role__c ='Secondary Technician' and 
            {  
                if( (userId1!=null && userId1.Id == co.Id) || (userId2!=null && userId2.Id == co.Id) || (userId3!=null && userId3.Id == co.Id) ||(userId5!=null && userId5.Id == co.Id) ) {
                    continue;
                }
                getSecondaryTechNames.add(new SelectOption(co.id,co.Name+' '+'('+co.Role__c+')')); 
            }
            return getSecondaryTechNames; 
       
    } 
    
    public SelectOption[] getDeputyOptions() 
    { 
        
            SelectOption[] getDeputyNames= new SelectOption[]{};  
                getDeputyNames.add(new SelectOption('','Select'));
               
                
            for (Contact co : [select id,Name,role__c  from Contact where Id in : conIds AND AccountID=:u.Contact.AccountId order by name]) //role__c ='Secondary Technician' and 
            {  
                if( (userId1!=null && userId1.Id == co.Id) || (userId2!=null && userId2.Id == co.Id) || (userId3!=null && userId3.Id == co.Id) ||(userId4!=null && userId4.Id == co.Id) ) {
                    continue;
                }
                getDeputyNames.add(new SelectOption(co.id,co.Name+' '+'('+co.Role__c+')')); 
            }
            return getDeputyNames; 
       
    } 
       
     
    public SelectOption[] getBuildingOwnerOptions() 
    { 
        
            SelectOption[] getOwnerNames= new SelectOption[]{};  
                getOwnerNames.add(new SelectOption('','--None--'));
            for (Building_Owner__c b : [select id,Name from Building_Owner__c order by name]) 
            {  
                getOwnerNames.add(new SelectOption(b.id,b.Name));
            }
            return getOwnerNames; 
       
        
    }
     public List<SelectOption> getTimezones()
    {
      List<SelectOption> options = new List<SelectOption>();
        
         Schema.DescribeFieldResult fieldResult = building__c.Time_Zone__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
           options.add(new SelectOption('','Select Time Zone'));
           for( Schema.PicklistEntry f : ple)
           {
           
              options.add(new SelectOption(f.getLabel(), f.getValue()));
           }
                  system.debug('options****'+options);
           return options;
     }
    
    public PageReference cancel()
    {
        PageReference n = new PageReference('/lcbs_moreinfo_buildinfo?id='+fetchinfo.id);
        n.setRedirect(true);
        return n; 
    }  
    
    public PageReference save() 
    {
        try{     
           // Site__c s = new Site__c();
            //s.Name = site;
           // insert s;
           // fetchinfo.Site__c = s.id;     
           // fetchinfo.Address__c = Address1+' '+Address2+' '+Address3;
            update fetchinfo;
            PageReference newb = new PageReference('/lcbs_moreinfo_buildinfo?id='+fetchinfo.id);
            newb.setRedirect(true);
            return newb;                     
        }
        catch (DmlException e) {
            ApexPages.addMessages(e);
        }
        return null;        
    }    
    
     public void getUserId1()
    {
    system.debug('##########'+fetchinfo.Dispatcher__c);
     if(fetchinfo.Dispatcher__c !=NULL )
      
     userId1 = [Select id,name from Contact where id=:fetchinfo.Dispatcher__c];
     
     else
     userId1=NULL;
    }
    public void getUserId2()
    {
     if(fetchinfo.Alternate_Dispatcher__c!=NULL )
     userId2 = [Select id,name from Contact where id=:fetchinfo.Alternate_Dispatcher__c];
     else
     userId2=NULL;
    }
    public void getUserId3()
    {
     if(fetchinfo.Technician__c!=NULL )
     userId3 = [Select id,name from Contact where id=:fetchinfo.Technician__c];
     else
     userId3=NULL;
    }
    public void getUserId4()
    {
     if(fetchinfo.Alternate_Technician__c!=NULL )
     userId4 = [Select id,name from Contact where id=:fetchinfo.Alternate_Technician__c];
     else
     userId4=NULL;
    }
    public void getUserId5()
    {
     if(fetchinfo.User_5__c!=NULL )
     userId5 = [Select id,name from Contact where id=:fetchinfo.User_5__c];
     else
     userId5=NULL;
    }
    
    public pagereference equipmentPage()
{    
 //String equipment = 'tag:honeywell.com,2012:acs/system#_'+fetchinfo.id;
 //String equipment = fetchinfo.id;
 //Blob equipmentBlob = Blob.valueOf(equipment);
 //string convertedEquipment= EncodingUtil.base64Encode(equipmentBlob);
 PageReference p = new PageReference(LCBSParameters.Domain_URL+'/app/equipments/'+c.Contact.Account.id+'/'+fetchinfo.id);
 p.setRedirect(true);
 return p; 
}

public pagereference alertsPage()
{    
 //String equipment = 'tag:honeywell.com,2012:acs/system#_'+fetchinfo.id;
  //String equipment = fetchinfo.id;
 //Blob equipmentBlob = Blob.valueOf(equipment);
 //string convertedEquipment= EncodingUtil.base64Encode(equipmentBlob);
 PageReference p = new PageReference(LCBSParameters.Domain_URL+'/app/alerts/'+c.Contact.Account.id+'/'+fetchinfo.id);
 p.setRedirect(true);
 return p; 
}
public pagereference controlsPage()
{    
 //String equipment = 'tag:honeywell.com,2012:acs/system#_'+fetchinfo.id;
 // String equipment = fetchinfo.id;
 //Blob equipmentBlob = Blob.valueOf(equipment);
 //string convertedEquipment= EncodingUtil.base64Encode(equipmentBlob);
 PageReference p = new PageReference(LCBSParameters.Domain_URL+'/app/controls/'+c.Contact.Account.id+'/'+fetchinfo.id);
 p.setRedirect(true);
 return p; 
}
}