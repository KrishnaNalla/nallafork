@isTest
public with sharing class LCBS_BuildingTrigger_Test {
    public static testmethod void BuildingTriggerTest() {
        building__c bld = new building__c(Name = 'Test Building');
        insert bld;
        update bld;
        delete bld;
        undelete bld;
    }
}